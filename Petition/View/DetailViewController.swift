//
//  DetailViewController.swift
//  Petition
//
//  Created by Hariharan S on 15/05/24.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {
    
    // MARK: - Properties

    var webView: WKWebView!
    var detailItem: Petition?

    override func loadView() {
        self.webView = WKWebView()
        self.view = self.webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let detailItem = self.detailItem
        else { 
            return
        }

        let html = """
        <html>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
                h1 {
                    font-size: 24px;
                }
                p {
                    font-size: 16px;
                }
                .separator {
                    border-top: 1px solid #000;
                    margin-top: 5px;
                    margin-bottom: 20px;
                }
            </style>
        </head>
        <body>
        <h1>\(detailItem.title)</h1>
        <div class="separator"></div>
        <p>\(detailItem.body)</p>
        </body>
        </html>
        """
        self.webView.loadHTMLString(html, baseURL: nil)
    }
}
