//
//  RecentTableViewController.swift
//  Petition
//
//  Created by Hariharan S on 15/05/24.
//

import UIKit

class RecentTableViewController: UITableViewController {

    // MARK: - Properties
    
    private var petitions = [Petition]()
    private var filteredPetitions = [Petition]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadJSON()
        self.configureNavBar()
    }
}

// MARK: - Private Methods

private extension RecentTableViewController {
    func configureNavBar() {
        self.title = "Recent Petitions"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Credits",
            style: .plain,
            target: self,
            action: #selector(self.showCreditsAlert)
        )
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Search",
            style: .plain,
            target: self,
            action: #selector(self.showSearchAlert)
        )
    }
    
    func loadJSON() {
        let urlString = "https://www.hackingwithswift.com/samples/petitions-1.json"
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let data = try? Data(contentsOf: url) {
                    DispatchQueue.main.async {
                        self.parse(json: data)
                        return
                    }
                } else {
                    self.showError()
                }
            }.resume()
        } else {
            self.showError()
        }
    }
    
    func parse(json: Data) {
        let decoder = JSONDecoder()
        if let jsonPetitions = try? decoder.decode(
            Petitions.self,
            from: json
        ) {
            self.petitions = jsonPetitions.results
            self.filteredPetitions = self.petitions
        }
    }
    
    func showError() {
        let alertVC = UIAlertController(
            title: "Loading error",
            message: "There was a problem loading the feed; please check your connection and try again.",
            preferredStyle: .alert
        )
        alertVC.addAction(
            UIAlertAction(
                title: "OK",
                style: .default
            )
        )
        self.present(alertVC, animated: true)
    }
}

// MARK: - Objc Methods

@objc
private extension RecentTableViewController {
    func showCreditsAlert() {
        let alertVC = UIAlertController(
            title: nil,
            message: "Fetched the datas from the We The People API of the Whitehouse.",
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(title: "OK", style: .default)
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true)
    }
    
    func showSearchAlert() {
        let alertVC = UIAlertController(
            title: "Enter the text to search",
            message: nil,
            preferredStyle: .alert
        )
        alertVC.addTextField()
        let okAction = UIAlertAction(
            title: "Submit",
            style: .default
        ) { _ in
            guard let searchString = alertVC.textFields?[0].text,
                  !searchString.isEmpty
            else {
                self.filteredPetitions = self.petitions
                return
            }
            var searchedPetitions = [Petition]()
            for petition in self.petitions where petition.title.contains(searchString) || petition.body.contains(searchString) {
                searchedPetitions.append(petition)
            }
            self.filteredPetitions = searchedPetitions
        }
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel
        )
        alertVC.addAction(okAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true)
    }
}
// MARK: - UITableViewDataSource Conformance

extension RecentTableViewController {
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return self.filteredPetitions.count
    }

    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "TableViewCell",
            for: indexPath
        )
        let petition = self.filteredPetitions[indexPath.row]
        cell.textLabel?.text = petition.title
        cell.detailTextLabel?.text = petition.body
        return cell
    }
}

// MARK: - UITableViewDelegate Conformance

extension RecentTableViewController {
    override func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) { 
        let detailVC = DetailViewController()
        detailVC.detailItem = self.filteredPetitions[indexPath.row]
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}
