//
//  Petition.swift
//  Petition
//
//  Created by Hariharan S on 15/05/24.
//

import Foundation

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}

struct Petitions: Codable {
    var results: [Petition]
}
